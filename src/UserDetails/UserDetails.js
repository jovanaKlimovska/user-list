import React from 'react';
import Modal from "../Modal/Modal";

function UserDetails(props) {
    return (
        <div>
            <Modal title="User Details" onClose={props.onClose} show={props.show}>
                <h4>E-mail: {props.email}</h4>
                <h4>Employed at: {props.company}</h4>
                <h4>Address: {props.address}</h4>
                <h4>Phone number: {props.phone}</h4>
            </Modal>
        </div>
    )
}

export default UserDetails;