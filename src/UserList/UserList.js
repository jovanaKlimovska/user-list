import React, { useState } from 'react';
import users from "../data/users.json";
import UserListStyle from './UserListStyle.css';
import UserDetails from '../UserDetails/UserDetails';

function UserList() {
  const [name, setName] = useState('');
  const [foundUsers, setFoundUsers] = useState(users);
  const [showDetails, setShowDetails] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);

  const openDetails = (e) => {
    setShowDetails(true);
    setSelectedUser(e)
  }

  const closeDetails = () => {
    setShowDetails(false);
  }

  const filter = (e) => {
    const keyword = e.target.value;

    if (keyword.length >= 3) {
      const results = users.filter((user) => {
        return user.name.toLowerCase().includes(keyword.toLowerCase());
      });
      setFoundUsers(results);
    } else {
      setFoundUsers(users);
    }

    setName(keyword);
  };

  const sortUsers = () => {
    const sortedUsers = [...foundUsers]

    sortedUsers.sort((a, b) => {
      if (a.age < b.age) return -1
      return a.age > b.age ? 1 : 0
    })

    setFoundUsers(sortedUsers)
  }

  return (
    <div>
      <h1>Filter Users by Name</h1>
      <input
        className="input"
        type="search"
        value={name}
        onChange={filter}
        className="input"
        placeholder="Filter"
      />

      <button className="filter-button" onClick={sortUsers}>
        Sort by age
      </button>

      <div className="user-list">
        {foundUsers && foundUsers.length > 0 ? (
          foundUsers.map((user) => (
            <li key={user._id}>
              <div>
                {user.name}, <span>age: {user.age}</span>
                <button className="details" onClick={openDetails.bind(null, user)}>
                  Show Details
                </button>
              </div>
            </li>
          ))
        ) : (
          <h4 className="notification">Results are empty!</h4>
        )}
      </div>

      <UserDetails
        email={selectedUser && selectedUser.email}
        company={selectedUser && selectedUser.company}
        address={selectedUser && selectedUser.address}
        phone={selectedUser && selectedUser.phone}
        show={showDetails}
        onClose={closeDetails}
      />

      {
        foundUsers.length > 0 &&
        <h4 className="notification">
          Total number of users: {foundUsers.length}
        </h4>
      }
    </div>
  )
}

export default UserList;
